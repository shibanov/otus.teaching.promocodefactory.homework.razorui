using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.WebHost.Pages.Employees
{
    public class IndexModel : PageModel
    {
        private readonly IRepository<Employee> _employeeRepository;

        public IndexModel(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public IEnumerable<Employee> Employees { get; private set; }

        public async Task OnGetAsync()
        {
            Employees = await _employeeRepository.GetAllAsync();
        }
    }
}
