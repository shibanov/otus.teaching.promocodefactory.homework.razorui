using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.WebHost.Pages.Employees
{
    public class DeleteModel : PageModel
    {
        private readonly IRepository<Employee> _employeeRepository;

        public DeleteModel(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        [BindProperty]
        public Employee Employee { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Employee = await _employeeRepository.GetByIdAsync(id.Value);
            if (Employee == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Employee = await _employeeRepository.GetByIdAsync(id.Value);
            if (Employee != null)
            {
                await _employeeRepository.DeleteAsync(Employee);
            }

            return RedirectToPage("./Index");
        }
    }
}
