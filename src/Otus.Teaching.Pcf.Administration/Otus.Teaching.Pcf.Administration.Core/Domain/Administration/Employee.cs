﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public Guid RoleId { get; set; }
        public virtual Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}