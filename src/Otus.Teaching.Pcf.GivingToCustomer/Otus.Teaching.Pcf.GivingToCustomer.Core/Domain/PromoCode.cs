﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCode
        : BaseEntity
    {
        [Required]
        public string Code { get; set; }

        [Required]
        public string ServiceInfo { get; set; }

        [DataType(DataType.Date)]
        public DateTime BeginDate { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }
        
        public virtual Preference Preference { get; set; }

        public Guid PreferenceId { get; set; }
        
        public virtual ICollection<PromoCodeCustomer> Customers { get; set; }
    }
}