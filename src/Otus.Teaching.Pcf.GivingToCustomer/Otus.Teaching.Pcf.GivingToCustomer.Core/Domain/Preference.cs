﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Preference
        :BaseEntity
    {
        [Required]
        public string Name { get; set; }
    }
}