using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Customers
{
    public class IndexModel : PageModelBase<Customer>
    {
        public IndexModel(IRepository<Customer> repository)
            : base(repository, PageModelType.Index)
        {
        }
    }
}
