using System.Collections.Generic;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Customers.Preferences
{
    public class DeleteModel : PageModelRelatedBase<Customer, CustomerPreference>
    {
        public DeleteModel(IRepository<Customer> repository)
            : base(repository, PageModelType.Delete)
        {
        }

        protected override ICollection<CustomerPreference> GetRelated(Customer master) => master.Preferences;
    }
}
