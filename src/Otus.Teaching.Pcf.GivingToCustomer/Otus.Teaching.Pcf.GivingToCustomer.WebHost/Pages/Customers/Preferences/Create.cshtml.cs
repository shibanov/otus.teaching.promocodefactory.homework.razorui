using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Customers.Preferences
{
    public class CreateModel : PageModelRelatedBase<Customer, CustomerPreference>
    {
        private readonly IRepository<Preference> _preferenceRepository;
        
        public CreateModel(IRepository<Customer> repository, IRepository<Preference> preferenceRepository)
            : base(repository, PageModelType.Create)
        {
            _preferenceRepository = preferenceRepository;
        }

        protected override ICollection<CustomerPreference> GetRelated(Customer master) => master.Preferences;

        protected override async Task OnGetCreateAsync(Guid? masterId)
        {
            ViewData["PreferenceId"] = new SelectList(await _preferenceRepository.GetAllAsync(), "Id", "Name");
        }

        protected override async Task OnModelNotValidAsync(Guid? masterId, Guid? id)
        {
            ViewData["PreferenceId"] = new SelectList(await _preferenceRepository.GetAllAsync(), "Id", "Name");
        }

        protected override Task OnPostCreateAsync(Guid? masterId)
        {
            Item.Id = Guid.NewGuid();
            Item.CustomerId = Master.Id;
            
            return Task.CompletedTask;
        }
    }
}
