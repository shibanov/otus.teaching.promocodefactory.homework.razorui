using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.PromoCodes
{
    public class DeleteModel : PageModelBase<PromoCode>
    {
        public DeleteModel(IRepository<PromoCode> repository)
            : base(repository, PageModelType.Delete)
        {
        }
    }
}
