using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.PromoCodes
{
    public class EditModel : PageModelBase<PromoCode>
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public EditModel(IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository)
            : base(promoCodeRepository, PageModelType.Edit)
        {
            _preferenceRepository = preferenceRepository;
        }

        protected override async Task OnGetEditAsync(Guid? id)
        {
            ViewData["PreferenceId"] = new SelectList(await _preferenceRepository.GetAllAsync(), "Id", "Name");
        }
    
        protected override async Task OnModelNotValidAsync(Guid? id)
        {
            ViewData["PreferenceId"] = new SelectList(await _preferenceRepository.GetAllAsync(), "Id", "Name");
        }
    }
}
