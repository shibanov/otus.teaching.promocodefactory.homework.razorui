namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions
{
    public enum PageModelType
    {
        Index,
        Details,
        Create,
        Edit,
        Delete
    }
}