using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions
{
    public abstract class PageModelRelatedBase<T, TRelated> : PageModel
        where T : BaseEntity
        where TRelated : BaseEntity
    {
        private readonly IRepository<T> _repository;
        private readonly PageModelType _pageModelType;

        public PageModelRelatedBase(IRepository<T> repository, PageModelType pageModelType)
        {
            _repository = repository;
            _pageModelType = pageModelType;
        }

        public T Master { get; protected set; }
        public IEnumerable<TRelated> Items { get; protected set;  }
        
        [BindProperty]
        public TRelated Item { get; set; }
        
        protected IRepository<T> Repository => _repository;

        protected PageModelType ModelType => _pageModelType;

        public async Task<IActionResult> OnGetAsync(Guid? masterId, Guid? id)
        {
            switch (_pageModelType)
            {
                case PageModelType.Index:
                    await GetItems(masterId);
                    await OnGetIndexAsync(masterId);
                    break;
                case PageModelType.Details:
                    if (!await GetItemById(masterId, id))
                        return NotFound();
                    await OnGetDetailsAsync(masterId, id);
                    break;
                case PageModelType.Create:
                    if (!await GetMasterById(masterId))
                        return NotFound();
                    await OnGetCreateAsync(masterId);
                    break;
                case PageModelType.Edit:
                    if (!await GetItemById(masterId, id))
                        return NotFound();
                    await OnGetEditAsync(masterId, id);
                    break;
                case PageModelType.Delete:
                    if (!await GetItemById(masterId, id))
                        return NotFound();
                    await OnGetDeleteAsync(masterId, id);
                    break;
            }

            return Page();
        }
        
        protected async Task<bool> GetMasterById(Guid? masterId)
        {
            if (masterId == null)
                return false;

            Master = await _repository.GetByIdAsync(masterId.Value);
            return Master != null;
        }

        protected async Task GetItems(Guid? masterId)
        {
            if (masterId == null)
                return;

            (Master, Items) = await _repository.GetAllRelatedAsync(masterId.Value, GetRelated);
        }
        
        protected async Task<bool> GetItemById(Guid? masterId, Guid? id)
        {
            if (masterId == null || id == null)
                return false;

            (Master, Item) = await _repository.GetRelatedByIdAsync(masterId.Value, GetRelated, id.Value);
            return Item != null;
        }

        protected abstract ICollection<TRelated> GetRelated(T master);
        
        protected virtual void UpdateRelated(TRelated entity, TRelated source)
        {}

#pragma warning disable 1998
        protected virtual async Task OnGetIndexAsync(Guid? masterId)
        {}

        protected virtual async Task OnGetDetailsAsync(Guid? masterId, Guid? id)
        {}

        protected virtual async Task OnGetCreateAsync(Guid? masterId)
        {}

        protected virtual async Task OnGetEditAsync(Guid? masterId, Guid? id)
        {}

        protected virtual async Task OnGetDeleteAsync(Guid? masterId, Guid? id)
        {}
#pragma warning restore 1998

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(Guid? masterId, Guid? id)
        {
            if (_pageModelType != PageModelType.Delete && !ModelState.IsValid)
            {
                await OnModelNotValidAsync(masterId, id);
                return Page();
            }

            var item = default(TRelated);
            if (_pageModelType == PageModelType.Create)
            {
                if (masterId == null)
                    return NotFound();
                Master = await _repository.GetByIdAsync(masterId.Value);
                if (Master == null)
                    return NotFound();
            }
            else
            {
                if (masterId == null || id == null)
                    return NotFound();
                (Master, item) = await _repository.GetRelatedByIdAsync(masterId.Value, GetRelated, id.Value);
                if (item == null)
                    return NotFound();
            }

            switch (_pageModelType)
            {
                case PageModelType.Create:
                    GetRelated(Master)?.Add(Item);
                    await OnPostCreateAsync(masterId);
                    await _repository.UpdateAsync(Master);
                    break;
                case PageModelType.Edit:
                    try
                    {
                        UpdateRelated(item, Item);
                        await OnPostEditAsync(masterId, id);
                        await _repository.UpdateAsync(Master);
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        var found = await _repository.GetByIdAsync(Master.Id);
                        if (found == null)
                            return NotFound();
                        throw;
                    }
                    break;
                case PageModelType.Delete:
                    GetRelated(Master)?.Remove(item);
                    await OnPostDeleteAsync(masterId, id);
                    await _repository.UpdateAsync(Master);
                    break;
            }

            return RedirectToPage("../Details", new { Id = masterId.Value });
        }

#pragma warning disable 1998
        protected virtual async Task OnModelNotValidAsync(Guid? masterId, Guid? id)
        {}

        protected virtual async Task OnPostCreateAsync(Guid? masterId)
        {}

        protected virtual async Task OnPostEditAsync(Guid? masterId, Guid? id)
        {}

        protected virtual async Task OnPostDeleteAsync(Guid? masterId, Guid? id)
        {}
#pragma warning restore 1998
    }
}