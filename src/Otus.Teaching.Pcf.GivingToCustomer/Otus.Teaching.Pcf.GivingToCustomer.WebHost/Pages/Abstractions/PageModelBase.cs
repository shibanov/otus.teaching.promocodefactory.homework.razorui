using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions
{
    public abstract class PageModelBase<T> : PageModel
        where T : BaseEntity
    {
        private readonly IRepository<T> _repository;
        private readonly PageModelType _pageModelType;

        public PageModelBase(IRepository<T> repository, PageModelType pageModelType)
        {
            _repository = repository;
            _pageModelType = pageModelType;
        }

        public IEnumerable<T> Items { get; protected set;  }
        
        [BindProperty]
        public T Item { get; set;  }
        
        protected IRepository<T> Repository => _repository;

        protected PageModelType ModelType => _pageModelType;

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            switch (_pageModelType)
            {
                case PageModelType.Index:
                    await GetItems();
                    await OnGetIndexAsync();
                    break;
                case PageModelType.Details:
                    if (!await GetItemById(id))
                        return NotFound();
                    await OnGetDetailsAsync(id);
                    break;
                case PageModelType.Create:
                    await OnGetCreateAsync();
                    break;
                case PageModelType.Edit:
                    if (!await GetItemById(id))
                        return NotFound();
                    await OnGetEditAsync(id);
                    break;
                case PageModelType.Delete:
                    if (!await GetItemById(id))
                        return NotFound();
                    await OnGetDeleteAsync(id);
                    break;
            }
            return Page();
        }

        protected async Task GetItems()
        {
            Items = await _repository.GetAllAsync();
        }
        
        protected async Task<bool> GetItemById(Guid? id)
        {
            if (id == null)
                return false;
            
            Item = await _repository.GetByIdAsync(id.Value);
            return Item != null;
        }
        
#pragma warning disable 1998
        protected virtual async Task OnGetIndexAsync()
        { }

        protected virtual async Task OnGetDetailsAsync(Guid? id)
        { }

        protected virtual async Task OnGetCreateAsync()
        { }

        protected virtual async Task OnGetEditAsync(Guid? id)
        { }

        protected virtual async Task OnGetDeleteAsync(Guid? id)
        { }
#pragma warning restore 1998
        
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (_pageModelType != PageModelType.Delete && !ModelState.IsValid)
            {
                await OnModelNotValidAsync(id);
                return Page();
            }

            switch (_pageModelType)
            {
                case PageModelType.Create:
                    await OnPostCreateAsync();
                    await _repository.AddAsync(Item);
                    break;
                case PageModelType.Edit:
                    try
                    {
                        await OnPostEditAsync(id);
                        await _repository.UpdateAsync(Item);
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        var found = await _repository.GetByIdAsync(Item.Id);
                        if (found == null)
                            return NotFound();
                        throw;
                    }
                    break;
                case PageModelType.Delete:
                    if (id == null)
                        return NotFound();
                    Item = await _repository.GetByIdAsync(id.Value);
                    if (Item != null)
                    {
                        await OnPostDeleteAsync(id);
                        await _repository.DeleteAsync(Item);
                    }
                    break;
            }

            return RedirectToPage("./Index");
        }
        
#pragma warning disable 1998
        protected virtual async Task OnModelNotValidAsync(Guid? id)
        { }

        protected virtual async Task OnPostCreateAsync()
        { }

        protected virtual async Task OnPostEditAsync(Guid? id)
        { }

        protected virtual async Task OnPostDeleteAsync(Guid? id)
        { }
#pragma warning restore 1998
    }
}