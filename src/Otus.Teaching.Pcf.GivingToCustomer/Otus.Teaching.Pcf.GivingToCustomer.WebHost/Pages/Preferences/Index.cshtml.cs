using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Preferences
{
    public class IndexModel : PageModelBase<Preference>
    {
        public IndexModel(IRepository<Preference> repository)
            : base(repository, PageModelType.Index)
        {
        }
    }
}
