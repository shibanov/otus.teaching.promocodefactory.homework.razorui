using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Preferences
{
    public class DeleteModel : PageModelBase<Preference>
    {
        public DeleteModel(IRepository<Preference> repository)
            : base(repository, PageModelType.Delete)
        {
        }
    }
}
