using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Abstractions;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages.Preferences
{
    public class EditModel : PageModelBase<Preference>
    {
        public EditModel(IRepository<Preference> repository)
            : base(repository, PageModelType.Edit)
        {
        }
    }
}
